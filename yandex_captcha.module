<?php

/**
 * @file
 * Yandex.Captcha module. Uses captcha from the Yandex.CleanWeb web service
 * to improve the CAPTCHA system.
 */

/**
 * Implements hook_help().
 */
function yandex_captcha_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/modules#name':
      $output .= t('Yandex.Captcha');
      break;

    case 'admin/modules#description':
    case 'admin/user/captcha/yandex_captcha':
      $output .= t('Uses the !url web service to improve the CAPTCHA module.', array(
        '!url' => l(t('Yandex.CleanWeb'), 'http://api.yandex.ru/cleanweb', array('attributes' => array('target' => '_blank'))),
      ));
      break;

    case 'admin/help#yandex_captcha':
      $output .= '<p>' .
        t('Uses the Yandex.CleanWeb web service to improve the CAPTCHA module. For more information on what Yandex.CleanWeb is, visit the !url.', array(
          '!url' => l(t('official website'), 'http://api.yandex.ru/cleanweb', array('attributes' => array('target' => '_blank'))),
        )) . '</p><h3>' .
        t('Configuration') .
        '</h3><p>' .
        t('The settings associated with Yandex.Captcha can be found in the !yandexcaptchatab tab, in the !captchasettings. You must set your Yandex.CleanWeb key to use the module. Once the key are set, visit the !captchasettings, where you can choose where Yandex.Captcha should be displayed.', array(
          '!yandexcaptchatab' => l(t('Yandex.Captcha'), 'admin/user/captcha/yandex_captcha'),
          '!captchasettings' => l(t('CAPTCHA settings'), 'admin/user/captcha'),
        )) . '</p>';
      break;

  }
  return $output;
}

/**
 * Implements hook_menu().
 */
function yandex_captcha_menu() {
  $items = array();
  $items['admin/config/people/captcha/yandex_captcha'] = array(
    'title' => 'Yandex.Captcha',
    'description' => 'Administer the Yandex.Captcha web service.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('yandex_captcha_admin_settings'),
    'access arguments' => array('administer yandex captcha'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'yandex_captcha.admin.inc',
  );
  $items['captcha/yandex_captcha_refresh'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'yandex_captcha_ajax_refresh',
    'page arguments' => array(2),
    'access callback' => TRUE,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function yandex_captcha_permission() {
  return array(
    'administer yandex captcha' => array(
      'title' => t('Yandex.Captcha Administration'),
      'description' => t('Administer Yandex.Captcha settings.'),
    ),
  );
}

/**
 * Implements hook_captcha().
 */
function yandex_captcha_captcha($op, $captcha_type = '') {
  global $user, $is_https;
  $yandex_captcha_key = variable_get('yandex_captcha_key', NULL);

  switch ($op) {
    case 'list':
      if (!empty($yandex_captcha_key)) {
        return array('Yandex.Captcha');
      }

    case 'generate':
      $result = array();
      if ($captcha_type == 'Yandex.Captcha') {

        if (!$yandex_captcha_key || !yandex_captcha_ping_server() || (variable_get('maintenance_mode', 0) && $user->uid == 0)) {
          return captcha_captcha('generate', 'Math');
        }

        $yandex_captcha_get_captcha = yandex_captcha_get_captcha();

        if (!$yandex_captcha_get_captcha OR empty($yandex_captcha_get_captcha['url']) OR empty($yandex_captcha_get_captcha['captcha'])) {
          watchdog('Yandex.Captcha', 'Failed to get the image URL and other data from the Yandex.CleanWeb server for an unknown reason', array(), WATCHDOG_ERROR);
          return captcha_captcha('generate', 'Math');
        }

        $result['solution'] = $yandex_captcha_get_captcha['captcha'];
        $result['captcha_validate'] = 'yandex_captcha_check_captcha';

        $result['form']['captcha_image'] = array(
          '#markup' => '<div class="yandex_captcha_image">' . theme('image', array(
            'path' => empty($is_https) ? $yandex_captcha_get_captcha['url'] : str_replace('http://', 'https://', $yandex_captcha_get_captcha['url']),
            'width' => 200,
            'height' => 60,
            'alt' => t('Image with code'),
            'title' => t('Type the code from picture.'),
          )) . '</div>',
        );

        $result['form']['captcha_response'] = array(
          '#type' => 'textfield',
          '#description' => t('Enter the validation text.'),
          '#size' => 27,
          '#maxlength' => 200,
          '#required' => TRUE,
        );

        if (variable_get('yandex_captcha_refresh_image', FALSE)) {
          $result['form']['#attached']['js'][] = drupal_get_path('module', 'yandex_captcha') . '/yandex_captcha.js';
        }

      }

      return $result;

  }
}

/**
 * Implements hook_element_info_alter().
 */
function yandex_captcha_element_info_alter(&$element) {
  if (isset($element['captcha'])) {
    $element['captcha']['#after_build'][] = 'yandex_captcha_after_build_process';
  }
}

/**
 * Add Yandex.Captcha refresh link to captcha form element.
 * @return element
 *   The process element
 */
function yandex_captcha_after_build_process($element, $form_state) {
  $form_id = $element['#captcha_info']['form_id'];
  $captcha_point = captcha_get_form_id_setting($form_id);
  if ($captcha_point && $captcha_point->captcha_type && $captcha_point->captcha_type == 'Yandex.Captcha' && variable_get('yandex_captcha_refresh_image', FALSE) && isset($element['captcha_widgets']['captcha_image'])) {
    $element['captcha_widgets']['captcha_refresh'] = array(
      '#markup' => '<div class="reload-yandex-captcha-wrapper">' . l(t('Show other validation text'), 'captcha/yandex_captcha_refresh/' . $form_id, array('attributes' => array('class' => array('reload-captcha')))) . '</div>',
    );
  }
  return $element;
}

/**
 * Whether or not the Yandex.CleanWeb server is up.
 * @return bool
 *   If server is up - return TRUE.
 */
function yandex_captcha_ping_server() {
  $request = yandex_captcha_http_request(array(
    'request_type' => 'ping',
  ));

  if (empty($request)) {
    return FALSE;
  }

  $result = preg_split('/\\r\\n?|\\n/', $request->data, 3);
  $result = new SimpleXMLElement($result[1]);

  if (isset($result->ok)) {
    return TRUE;
  }

  if (isset($result->message)) {
    watchdog('Yandex.Captcha', 'The connection to the Yandex.CleanWeb server is not possible for the following reason: %answer', array(
      '%answer' => $result->message,
    ), WATCHDOG_ERROR);
  }

  return FALSE;
}

/**
 * Get captcha from the Yandex.CleanWeb server.
 * @return array
 *   Array with captcha code and url to image with captcha.
 */
function yandex_captcha_get_captcha() {
  $request = yandex_captcha_http_request(array(
    'request_type' => 'get-captcha',
    'type' => variable_get('yandex_captcha_type', 'std'),
  ));

  if (empty($request)) {
    watchdog('Yandex.Captcha', 'Unable to understand answer after try to get captcha from the Yandex.CleanWeb server', array(), WATCHDOG_ERROR);
    return FALSE;
  }

  $result = preg_split('/\\r\\n?|\\n/', $request->data, 3);
  $result = new SimpleXMLElement($result[1]);

  if (!empty($result->captcha) && !empty($result->url) && valid_url($result->url, TRUE)) {
    return array(
      'captcha' => $result->captcha,
      'url' => $result->url,
    );
  }

  watchdog('Yandex.Captcha', 'Unable to understand answer from the Yandex.CleanWeb server', array(), WATCHDOG_ERROR);
  return FALSE;
}

/**
 * Check captcha on the Yandex.CleanWeb server.
 * @return bool
 *   Return TRUE if user fill right captcha text.
 */
function yandex_captcha_check_captcha($captcha, $value) {
  $request = yandex_captcha_http_request(array(
    'request_type' => 'check-captcha',
    'type' => variable_get('yandex_captcha_type', 'std'),
    'captcha' => $captcha,
    'value' => $value,
  ));

  if (empty($request)) {
    watchdog('Yandex.Captcha', 'Unable to understand answer after check captcha on the Yandex.CleanWeb server', array(), WATCHDOG_ERROR);
    return FALSE;
  }

  $result = preg_split('/\\r\\n?|\\n/', $request->data, 3);
  $result = new SimpleXMLElement($result[1]);

  if (isset($result->failed)) {
    return FALSE;
  }

  if (isset($result->ok)) {
    return TRUE;
  }

  watchdog('Yandex.Captcha', 'Unable to understand answer after check captcha on the Yandex.CleanWeb server', array(), WATCHDOG_ERROR);
  return FALSE;
}

/**
 * HTTP request to Yandex.CleanWeb API server.
 * @return object
 *   Return object with answer or FALSE if an error occurred.
 */
function yandex_captcha_http_request($urn = FALSE) {
  if (empty($urn) OR empty($urn['request_type'])) {
    watchdog('Yandex.Captcha', 'The required parameters are not specified for yandex_captcha_http_request function.', array(), WATCHDOG_ERROR);
    return FALSE;
  }

  $urn['key'] = variable_get('yandex_captcha_key', NULL);
  if (empty($urn['key'])) {
    watchdog('Yandex.Captcha', 'The key is not available. You must specify a key on setting of !yandexcaptchatab. Otherwise the request is impossible.', array(
      '!yandexcaptchatab' => l(t('Yandex.Captcha'), 'admin/user/captcha/yandex_captcha'),
    ), WATCHDOG_ERROR);
    return FALSE;
  }

  $request_url = 'http://cleanweb-api.yandex.ru/1.0/' . $urn['request_type'];
  unset($urn['request_type']);
  $url = url($request_url, array('query' => $urn, 'absolute' => TRUE));
  $request = drupal_http_request($url);

  if (empty($request->status_message)) {
    $request->status_message = t('Unknown error.');
  }

  if (empty($request->code)) {
    $request->status_message = t('Unknown error. Probably the connection with Yandex is missing.');
  }

  if ($request->code != 200) {
    watchdog('Yandex.Captcha', 'Unable to connect with the Yandex.CleanWeb server: %errno: %errstr', array(
      '%errno' => $request->code,
      '%errstr' => $request->status_message,
    ), WATCHDOG_ERROR);
    return FALSE;
  }

  return $request;
}

/**
 * Yandex.Captcha refresh ajax handler.
 * @return json
 *   Data about refreshed captcha in json format.
 */
function yandex_captcha_ajax_refresh($form_id) {
  global $is_https;

  drupal_page_is_cacheable(FALSE);
  module_load_include('inc', 'captcha');
  $result = array('status' => 0, 'message' => '');

  $yandex_captcha_get_captcha = yandex_captcha_get_captcha();

  if (!$yandex_captcha_get_captcha OR empty($yandex_captcha_get_captcha['url']) OR empty($yandex_captcha_get_captcha['captcha'])) {
    watchdog('Yandex.Captcha', 'Failed to get the image URL and other data from the Yandex.CleanWeb server for an unknown reason', array(), WATCHDOG_ERROR);
    $result['message'] = t('Error has occured. Please contact the site administrator.');
  }
  else {
    $captcha_sid = _captcha_generate_captcha_session($form_id);
    $captcha_token = md5(mt_rand());

    db_update('captcha_sessions')
      ->fields(array('token' => $captcha_token, 'solution' => $yandex_captcha_get_captcha['captcha']))
      ->condition('csid', $captcha_sid)
      ->execute();

    $result['data'] = array(
      'url' => empty($is_https) ? $yandex_captcha_get_captcha['url'] : str_replace('http://', 'https://', $yandex_captcha_get_captcha['url']),
      'token' => $captcha_token,
      'sid' => $captcha_sid,
    );
    $result['status'] = 1;
  }

  drupal_json_output($result);
}
